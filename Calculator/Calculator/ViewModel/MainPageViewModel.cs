﻿using Calculator.Utils;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using Calculator.Model;

namespace Calculator.ViewModel
{
    class MainPageViewModel : ObservableObject
    {
        CalculatorModel calculatorModel;

        public Command EqualsCommand { get; }

        public Command AddCommand { get; }

        private string display;

        public string Display
        {
            get
            {
                return display;
            }
            set
            {
                display = value;
            }
        }

        public MainPageViewModel()
        {
            calculatorModel = new CalculatorModel();
            EqualsCommand = new Command(EqualsCommandExecute);
            AddCommand = new Command(AddCommandExecute);
        }

        private void EqualsCommandExecute()
        {
            display = calculatorModel.Result.ToString();
            OnPropertyChanged(nameof(Display));
        }

        private void AddCommandExecute()
        {
            calculatorModel.AddEquation(new Equation(decimal.Parse(display), MathOperator.Add));
            display = calculatorModel.Result.ToString();
            OnPropertyChanged(nameof(Display));
        }
    }
}
