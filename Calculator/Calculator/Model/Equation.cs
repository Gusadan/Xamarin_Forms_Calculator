﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Calculator.Model
{
    public enum MathOperator
    {
        Add,
        Multiply,
        Subtract,
        Divide
    }
    
    // NOTE : Not a good class name (misleading) I think.
    class Equation
    {
        private decimal variable;

        public decimal Variable
        {
            get { return variable; }
            set { variable = value; }
        }

        public MathOperator MathOperator { get; set; }

        public Equation(decimal variable, MathOperator mathOperator)
        {
            this.variable = variable;
            this.MathOperator = MathOperator;
        }
    }
}