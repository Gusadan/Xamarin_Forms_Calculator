﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Calculator.Model
{
    class CalculatorModel
    {
        public decimal Result { get; set; }

        private List<Equation> equations;

        public CalculatorModel()
        {
            equations = new List<Equation>();
        }

        public void AddEquation(Equation equation)
        {
            // Add the variable in the last item on the list to the new variable if the list is not empty
            if (equations.Count > 0)
                this.Result = Result + equation.Variable;
            else
                this.Result = equation.Variable;
            equations.Add(equation);
        }
    }
}